package bank;

public class Bank {
 public int number;
 public double balance;
 public String currency;
 
 
 public double getbalance() {
	 return balance;
 } 
 public int getnumber() {
	 return number;
 } 
 public String getcurrency() {
	 return currency;
 } 
 
 public void getInfo() {
	 System.out.println("The account number "+number+" has "+balance+currency+" in it");
 }
 public void deposit(double amount) {
 balance+=amount;
 }
 public void withdraw(double amount) {
	 balance-=amount;
 }
 public void SetBalance (double amount) {
	 this.balance=amount;
 }
 public void SetNumber (int num) {
	 this.number=num;
 }
 public void SetCurrency (String c) {
	 this.currency=c;
 }
}
